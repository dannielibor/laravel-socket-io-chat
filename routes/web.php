<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/user', function () {
    return view('welcome');
});
Route::resource('/workspaces', 'WorkspaceController');
Route::post('/createworkspace', 'WorkspaceController@store');
Route::post('/post-message', 'ChatController@postMessage');
Route::resource('room', 'RoomController');
Route::get('/createroom', 'RoomController@store');
