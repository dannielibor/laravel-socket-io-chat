<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Workspace extends Model
{
    public function workspace_user(){
        return $this->hasMany('App\Workspace_users');
    }
}
