<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redis;
use App\User;
use App\Message;

class ChatController extends Controller
{
    public function postMessage(Request $request)
    {
        $user = User::find(auth()->user()->id);
        $response = ['message' => $request->message, 'user' => $user->name];
        $redis = Redis::connection();
        $redis->publish('message', json_encode($response));
        return response()->json($response, 200);
    }
}
