<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Workspace;
use App\Workspace_user;
use Auth;
use App\Room;
use App\Room_user;
use App\User;
use App\Http\Resources\Room as RoomResource;

class WorkspaceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $workspace = Workspace_user::where('user_id', auth()->user()->id)->get();
        return view('workspaces')->with('workspaces', $workspace);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $workspace = Workspace::where('name', $request->name)->get();
        if(count($workspace) == 0){
            $workspace = new Workspace();
            $workspace->name = $request->name;

            $workspace->save();

            $workspace = Workspace::where('name', $request->name)->first();

            $workspace_user = new Workspace_user();
            $workspace_user->workspace_id = $workspace->id;
            $workspace_user->user_id = auth()->user()->id;
            $workspace_user->isAdmin = 1;

            $room = new Room();
            $room->workspace_id = $workspace->id;

            $room->save();

            $room = Room::where('workspace_id', $workspace->id)->first();

            $roomUser = new Room_user();

            $roomUser->room_id = $room->id;
            $roomUser->user_id = auth()->user()->id;
            $roomUser->isAdmin = 1;

            $roomUser->save();

            $workspace_user->save();

            
        }
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $rooms = Room::paginate(10);
        // return RoomResource::collection($rooms);
        $rooms = Room::where('workspace_id', $id)->get();
        $workspace = Workspace::where('id', $id)->first();
        return view('welcome')->with('rooms', $rooms)->with('workspace', $workspace);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
