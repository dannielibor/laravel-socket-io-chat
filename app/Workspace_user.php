<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Workspace_user extends Model
{
    public function workspace(){
        return $this->belongsTo('App\Workspace');
    }
}
