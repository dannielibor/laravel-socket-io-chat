@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Workspaces</div>

                <div class="panel-body">
                    {!! Form::open(['action' => 'WorkspaceController@store', 'method' => 'POST']) !!}
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Workspace</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <button type="submit" class="btn btn-primary">
                                Create
                            </button>

                        </div>
                    {!!Form::close()!!}
                    <table class="table">
                            <thead>
                              <tr>
                                <th scope="col"></th>
                              </tr>
                            </thead>
                            <tbody>
                            
                                  @if(count($workspaces) > 0)
                                    @foreach($workspaces as $workspace)
                                    <tr>
                                        <td>
                                            <a href="/workspaces/{{$workspace->workspace->id}}">{{$workspace->workspace->name}}</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                  @endif
                              
                            </tbody>
                          </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
